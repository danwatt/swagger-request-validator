package com.atlassian.oai.validator;

import com.atlassian.oai.validator.model.Request;
import com.atlassian.oai.validator.model.Response;
import com.atlassian.oai.validator.model.SimpleRequest;
import com.atlassian.oai.validator.model.SimpleResponse;
import com.atlassian.oai.validator.report.LevelResolver;
import org.junit.Test;

import static com.atlassian.oai.validator.report.ValidationReport.Level.IGNORE;
import static com.atlassian.oai.validator.util.ValidatorTestUtil.assertPass;

/**
 * General behavioral tests for the {@link SwaggerRequestResponseValidator}.
 *
 * @see RequestValidationTest
 * @see ResponseValidationTest
 */
public class SwaggerRequestResponseValidatorTest {

    private final SwaggerRequestResponseValidator classUnderTest =
            SwaggerRequestResponseValidator.createFor("/oai/api-users.json").build();

    @Test(expected = NullPointerException.class)
    public void validate_withNullRequest_throwsNPE() {
        final Request request = null;
        final Response response = SimpleResponse.Builder.ok().build();

        classUnderTest.validate(request, response);
    }

    @Test(expected = NullPointerException.class)
    public void validate_withNullResponse_throwsNPE() {
        final Request request = SimpleRequest.Builder.get("/users").build();
        final Response response = null;

        classUnderTest.validate(request, response);
    }

    @Test
    public void validate_withFailures_shoudPass_whenLevelResolverIgnoresFailures() {
        final SwaggerRequestResponseValidator classUnderTest =
                SwaggerRequestResponseValidator
                        .createFor("/oai/api-users.json")
                        .withLevelResolver(LevelResolver
                                .create()
                                .withLoader(null)
                                .withDefaultLevel(IGNORE)
                                .build()
                        ).build();

        final Request request = SimpleRequest.Builder.get("/users/1").build();
        final Response response = SimpleResponse.Builder.ok().build();

        assertPass(classUnderTest.validate(request, response));
    }

    @Test
    public void validate_jsonPayloadAccepted() {
        SwaggerRequestResponseValidator.createFor("{}").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void validate_neitherPathNorJson() {
        SwaggerRequestResponseValidator.createFor("<>").build();
    }

    @Test(expected = NullPointerException.class)
    public void validate_withNullAuthHeaderKey_throwsNPE() throws Exception {
        SwaggerRequestResponseValidator
                .createFor("/oai/api-users.json")
                .withAuthHeaderData(null, null)
                .build();
    }

    @Test
    public void validate_withNullAuthHeaderValue() throws Exception {
        SwaggerRequestResponseValidator
                .createFor("/oai/api-users.json")
                .withAuthHeaderData("api-key", null)
                .build();
    }

    @Test
    public void validate_withBasePathOverride() throws Exception {
        final SwaggerRequestResponseValidator classUnderTest =
                SwaggerRequestResponseValidator
                        .createFor("/oai/api-users.json")
                        .withBasePathOverride("/test")
                        .build();

        final Request request = SimpleRequest.Builder
                .get("/test/users/1")
                .withHeader("Authorization", "Basic EncryptedUsernameAndPassword")
                .build();
        final Response response = SimpleResponse.Builder.ok().withBody("{\"id\":1,\"name\":\"Max\",\"email\":\"max@example.com\"}").build();

        assertPass(classUnderTest.validate(request, response));
    }
}
